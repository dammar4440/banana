﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelC : MonoBehaviour
{

    public static LevelC instance = null;
    GameObject levelSign, gameOverText, youWinText;
    int sceneIndex, levelPassed;
    public int scena;

    // Use this for initialization
    void Start()
    {

        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        levelSign = GameObject.Find("LevelNumber");
        gameOverText = GameObject.Find("GameOverText");
        youWinText = GameObject.Find("YouWinText");
        gameOverText.gameObject.SetActive(false);
        youWinText.gameObject.SetActive(false);

        sceneIndex = SceneManager.GetActiveScene().buildIndex-1;
        levelPassed = PlayerPrefs.GetInt("LevelPassed");
        scena = SceneManager.GetActiveScene().buildIndex;
    }

    public void youWin()
    {
        PlayerPrefs.SetInt("bestscore" + scena, 100);
        if (sceneIndex == 3)
            SceneManager.LoadScene("Menu");
        else
        {
            if (levelPassed < sceneIndex)
                PlayerPrefs.SetInt("LevelPassed", sceneIndex);
            levelSign.gameObject.SetActive(false);
            youWinText.gameObject.SetActive(true);
            SceneManager.LoadScene("Menu");
        }
    }

    public void youLose()
    {
        if (PlayerPrefs.GetInt("bestscore" + scena) < 100)
        {
            PlayerPrefs.SetInt("attempts" + scena, PlayerPrefs.GetInt("attempts" + scena) + 1);
        }
        levelSign.gameObject.SetActive(false);
        gameOverText.gameObject.SetActive(true);
        SceneManager.LoadScene(sceneIndex+1);
    }
    public void loadmenu() //dla przycisku w grze
    {
        SceneManager.LoadScene("Menu");
    }
}
