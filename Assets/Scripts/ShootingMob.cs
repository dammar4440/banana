﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingMob : MonoBehaviour
{
    public GameObject bullet;
    private Transform place;

    // Start is called before the first frame update
    void Start()
    {
        place = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Shoot()
    {
        Instantiate(bullet, new Vector3(place.transform.position.x - 0.4f, place.transform.position.y + 0.2f, place.transform.position.z), Quaternion.identity);
    }
}
