﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Sound : MonoBehaviour
{
    private AudioSource audioS;
    [SerializeField]
    private AudioClip menumusic;
    [SerializeField]
    private AudioClip levelmusic;
    private static Sound instance = null;
    public static Sound Instance

    {
        get { return instance; }
    }
    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        audioS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().buildIndex == 0 || SceneManager.GetActiveScene().buildIndex == 1)
        {
            if(audioS.clip!=menumusic)
            {
                audioS.clip = menumusic;
                audioS.Play();
            }
        }
        else
        {
            if(audioS.clip!=levelmusic)
            {
                audioS.clip = levelmusic;
                audioS.Play();
            }
        }
        
    }
}