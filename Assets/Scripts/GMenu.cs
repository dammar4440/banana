﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GMenu : MonoBehaviour
{
    private int sceneIndex;
    private Animator animator;
    [SerializeField]
    private GameObject image;
    [SerializeField]
    private GameObject enemylist;
    private bool creditsy;
    void Start()
    {
        sceneIndex = SceneManager.GetActiveScene().buildIndex;
        animator = GetComponent<Animator>();
        image.SetActive(false);
        creditsy = false;
        enemylist.SetActive(false);
    }

    public void loadMenu2()
    {
        SceneManager.LoadScene(sceneIndex + 1);
    }
    public void SettingsIn()
    {
        image.SetActive(true);
        animator.SetBool("OUT", false);
        animator.SetBool("IN", true);
        animator.SetBool("IDLE", false);
    }
    public void SettingsOut()
    {
        image.SetActive(false);
        animator.SetBool("OUT", true);
        animator.SetBool("IN", false);
        animator.SetBool("IDLE", true);
    }
    public void Credits()
    {
        if (creditsy == false)
        {
           // szarosc.SetActive(true);
            animator.SetBool("OUTC", false);
            animator.SetBool("INC", true);
            animator.SetBool("IDLE", false);
            creditsy = true;
        }
        else
        {
           // szarosc.SetActive(false);
            animator.SetBool("OUTC", true);
            animator.SetBool("INC", false);
            animator.SetBool("IDLE", true);
            creditsy = false;
        }
    }
    public void EnemyListOn()
    {
        enemylist.SetActive(true);
    }
    public void EnemyListOff()
    {
        enemylist.SetActive(false);
    }
}
