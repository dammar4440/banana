﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Pathfinding;

public class PlayerController : PhysicsObject
{
    public AIPath aIPath;
    public Transform osa;
    public AudioSource hit;
    public AudioSource gameover;
    public AudioSource jump;
    public AudioSource coins;
    public Transform player;
    public Slider jumpSlider;
    public float maxSpeed;
    public float jumpTakeOffSpeed;
    private bool lastposition;
    private float maxdistance;
    private float disstart;
    private float dissend;
    private float asdl;
    private float zycia;
    public Text LiczaZyc;
    [SerializeField]
    Image Zycie;
    [SerializeField]
    private Sprite spriteZycia;
    [SerializeField]
    private Sprite spriteZyciaPol;

    private SpriteRenderer spriteRenderer;
    private Animator animator;
    public string levlelname;
	public static PlayerController instance;
    

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();  
    }

    private void Start()
    {
        jumpTakeOffSpeed = 4;
        levlelname = SceneManager.GetActiveScene().name.ToString();
        aIPath.canSearch = false;
        InvokeRepeating("SavePlayer", 5f,5f);
        string path = Application.persistentDataPath + "/" + levlelname + ".save";
        if (File.Exists(path))
        {
            LoadPlayer();
        }
        LiczaZyc.text = PlayerPrefs.GetFloat("Zycia").ToString();
        if (PlayerPrefs.GetInt("LevelPassed") == 1)
            jumpTakeOffSpeed = 5;
        if (PlayerPrefs.GetInt("LevelPassed") == 2)
            jumpTakeOffSpeed = 6;
    }

    protected override void ComputeVelocity()
    {
        setDistance(0);
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump.Play();
            velocity.y = jumpTakeOffSpeed;
            
        }
        else if (Input.GetButtonUp("Jump"))
        {
            
            if (velocity.y > 0)
            {
                
                velocity.y = velocity.y * 0.5f;
            }
        }

       if (move.x > 0.01f)
        {
            spriteRenderer.flipX = false;
            lastposition = false;
        }
       else if (move.x < -0.01f)
        {
            spriteRenderer.flipX = true;
            lastposition = true;
        }
        else if (move.x == 0f)
        {
            spriteRenderer.flipX = lastposition;
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
        if (Input.GetButtonDown("Jump") && grounded)
        {
            disstart = getDistance();
        }
        else if (!grounded)
        {
            dissend = getDistance();
            asdl = dissend - disstart;
            setDistance(asdl / 1.78f);
        }

    }

    private float getDistance()
    {
        return player.transform.position.y;
    }
    private void setDistance(float distan)
    {
        jumpSlider.value =  distan;
    }

    public void SavePlayer()
    {
        if (grounded)
        {
            SaveSystem.SavePlayer(this);
        }

    }

    public void LoadPlayer()
    {
        Save save = SaveSystem.LoadPlayer(levlelname);
        Vector3 position;
        levlelname = save.levelname;
        position.x = save.position[0];
        position.y = save.position[1];
        position.z = save.position[2];
        transform.position = position;
    }

    public void LoadMenu()
    {
        SavePlayer();
        SceneManager.LoadScene("Menu");
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Kamienie")
        {
            przeszkodaTyp();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {  
        if (collision.tag == "Przeciwnik")
        {
            hit.Play();
            zycia = PlayerPrefs.GetFloat("Zycia");
            zycia -= 1f;
            PlayerPrefs.SetFloat("Zycia", zycia);
            getLives();
        }
        if (collision.tag == "Bananki")
        {
            coins.Play();
        }

        if (collision.tag == "Przeszkoda")
        {
            przeszkodaTyp();
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Osa")
        {
            aIPath.canSearch = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Osa")
        {
            aIPath.canSearch = false;
        }
    }
    private void getLives()
    {
        LiczaZyc.text = PlayerPrefs.GetFloat("Zycia").ToString();
        if (PlayerPrefs.GetFloat("Zycia") <= 0)
        {
            gameover.Play();
            SceneManager.LoadScene("Menu");
        }
    }
    private void przeszkodaTyp()
    {
        if (Zycie.sprite == spriteZycia)
        {
            hit.Play();
            Zycie.sprite = spriteZyciaPol;
            zycia = PlayerPrefs.GetFloat("Zycia");
            zycia -= 0.5f;
            PlayerPrefs.SetFloat("Zycia", zycia);
            getLives();
        }
        else if (Zycie.sprite == spriteZyciaPol)
        {
            hit.Play();
            Zycie.sprite = spriteZycia;
            zycia = PlayerPrefs.GetFloat("Zycia");
            zycia -= 0.5f;
            PlayerPrefs.SetFloat("Zycia", zycia);
            getLives();
        }
    }
}