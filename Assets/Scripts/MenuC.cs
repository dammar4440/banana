﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;

public class MenuC : MonoBehaviour
{

    public Button level01Button, level02Button, level03Button;
    int levelPassed;
    public GameObject Las, Pustynia;
    public GameObject Klucz1, Klucz2;
    [SerializeField]
    private GameObject Sklep;
    public Texture Las1, Las2, Pustynia1, Pustynia2;
    private RawImage las, pustynia;
    [SerializeField]
    private Text Zycia;
    [SerializeField]
    private Text Bananki;
    private int LBanankow;
    private float obecneZycie;
    private int obecneBananki;
    [SerializeField]
    private GameObject endgame;
    [SerializeField]
    private GameObject endgame2;
    [SerializeField]
    private Text InfoText;
    [SerializeField]
    private GameObject endgame2info;
    private string currentdata;
    [SerializeField]
    private GameObject dailyreward;

    void Start()
    {
        las = (RawImage)Las.GetComponent<RawImage>();
        pustynia = (RawImage)Pustynia.GetComponent<RawImage>();
        level01Button.onClick.AddListener(ButtonClickedMountains);
        level02Button.onClick.AddListener(ButtonClickedLas);
        level03Button.onClick.AddListener(ButtonClickedPustynia);
        Sklep.SetActive(false);
        endgame.SetActive(false);
        endgame2.SetActive(false);
        endgame2info.SetActive(false);
        InfoText.text = "Choose Level and start play!";
        dailyReward();        
    }
    private void Update()
    {
        SprawdzCzyMoznaGrac();
        if (PlayerPrefs.GetFloat("Zycia") > 0)
            Zycia.text = "x " + PlayerPrefs.GetFloat("Zycia").ToString();
        else
            Zycia.text = "x 0";
        Bananki.text = "x " + PlayerPrefs.GetInt("LiczbaBananow").ToString();

        if (PlayerPrefs.GetInt("LevelPassed") == 1)
        {
            Klucz1.SetActive(false);
            las.texture = (Texture)Las2;
        }
        if (PlayerPrefs.GetInt("LevelPassed") == 2)
        {
            Klucz1.SetActive(false);
            Klucz2.SetActive(false);
            las.texture = (Texture)Las2;
            pustynia.texture = (Texture)Pustynia2;
            endgame.SetActive(true);
            CheckEnd();
        }
    }
    private void CheckEnd()
    {
        if (PlayerPrefs.GetInt("LiczbaBananow") >= 80)
        {
            endgame.SetActive(false);
            endgame2.SetActive(true);
        }
    }
    public void ButtonClickedEndBefore80()
    {
        InfoText.text = "You heave not enough bananas to do that!";
    }
    public void ButtonClickedEnd()
    {
        endgame2info.SetActive(true);
    }
    public void ButtonClickedEndClose()
    {
        endgame2info.SetActive(false);
    }

    //metoda ładowania poziomu po kliknięciu w przycisk
    private void ButtonClickedMountains()
    {
        ResetZyc();
        SceneManager.LoadScene(2);
    }

    //metoda ładowania poziomu wraz z możliwością jego odblokowania po spełnieniu określonych warunków
    private void ButtonClickedLas()
    {
        levelPassed = PlayerPrefs.GetInt("LevelPassed");
        if (levelPassed != 1 && levelPassed != 2)
        {
            if (PlayerPrefs.GetInt("LiczbaBananow") >= 50)
            {
                LBanankow = PlayerPrefs.GetInt("LiczbaBananow");
                LBanankow -= 50;
                PlayerPrefs.SetInt("LiczbaBananow", LBanankow);
                ResetZyc();
                PlayerPrefs.SetInt("LevelPassed", 1);
                SceneManager.LoadScene(3);
            }
            else
            {
                InfoText.text = "You heave not enough bananas to do that!";
            }
        }
        if (levelPassed == 1 || levelPassed == 2)
        {
            ResetZyc();
            SceneManager.LoadScene(3);
        }
    }

    //metoda ładowania poziomu wraz z możliwością jego odblokowania po spełnieniu określonych warunków
    private void ButtonClickedPustynia()
    {
        levelPassed = PlayerPrefs.GetInt("LevelPassed");
        if (levelPassed == 1 && levelPassed != 2)
        {
            if (PlayerPrefs.GetInt("LiczbaBananow") >= 150)
            {
                LBanankow = PlayerPrefs.GetInt("LiczbaBananow");
                LBanankow -= 150;
                PlayerPrefs.SetInt("LiczbaBananow", LBanankow);
                ResetZyc();
                PlayerPrefs.SetInt("LevelPassed", 2);
                SceneManager.LoadScene(4);
            }
            else
            {
                InfoText.text = "You heave not enough bananas to do that!";
            }
        }
        if (levelPassed == 2)
        {
            ResetZyc();
            SceneManager.LoadScene(4);
        }
        else
        {
            InfoText.text = "First unlock level number 2!";
        }
    }

    //metoda resetowania dokonań gracza, jego pozycji na mapach, ogólnie całych statystyk
    public void resetPlayerPrefs()
    {
        las.texture = (Texture)Las1;
        pustynia.texture = (Texture)Pustynia1;
        level01Button.interactable = true;
        level02Button.interactable = false;
        level03Button.interactable = false;
        PlayerPrefs.DeleteAll();
        string datapath1 = Application.persistentDataPath + "/" + "LVL1" + ".save";
        if (File.Exists(datapath1))
        {
            File.Delete(datapath1);
        }
        string datapath2 = Application.persistentDataPath + "/" + "LVL2" + ".save";
        if (File.Exists(datapath2))
        {
            File.Delete(datapath2);
        }
        string datapath3 = Application.persistentDataPath + "/" + "LVL3" + ".save";
        if (File.Exists(datapath3))
        {
            File.Delete(datapath3);
        }
        ResetZyc();
        Klucz1.SetActive(true);
        Klucz2.SetActive(true);
        endgame.SetActive(false);
        endgame2.SetActive(false);
    }

    //metoda pójścia do menu głównego po kliknięciu w przycisk
    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }

    //metoda wyjścia z gry po kliknieciu przycisku
    public void wyjscie()
    {
        Application.Quit();
    }

    //metoda sprawdza czy gracz pierwszy raz gra w grę jeśli tak ustawia wartość żyć na 10
    private void ResetZyc()
    {
        if (PlayerPrefs.GetString("FirstGame") != "false")
        {
            PlayerPrefs.SetFloat("Zycia", 10);
            PlayerPrefs.SetString("FirstGame", "false");
        }
    }

    //metoda sprawdza wartość żyć, jeśli jest równa 0 lub mniejsza gracz nie może wejść na level, dopiero po zakupie lub dodaniu żyć będzie to możliwe
    private void SprawdzCzyMoznaGrac()
    {
        if (PlayerPrefs.GetFloat("Zycia") <= 0)
        {
            level01Button.interactable = false;
            level02Button.interactable = false;
            level03Button.interactable = false;
            InfoText.text = "Not enough lifes to play!";
        }
        else
        {
            level01Button.interactable = true;
            level02Button.interactable = true;
            level03Button.interactable = true;
        }
    }

    public void dodajZycie5()
    {
        obecneZycie = PlayerPrefs.GetFloat("Zycia") + 5;
        PlayerPrefs.SetFloat("Zycia", obecneZycie);
    }
    public void dodajZycie12()
    {
        obecneZycie = PlayerPrefs.GetFloat("Zycia") + 12;
        PlayerPrefs.SetFloat("Zycia", obecneZycie);
    }
    public void dodajBananki10()
    {
        obecneBananki = PlayerPrefs.GetInt("LiczbaBananow") + 10;
        PlayerPrefs.SetInt("LiczbaBananow", obecneBananki);
    }
    public void dodajBananki25()
    {
        obecneBananki = PlayerPrefs.GetInt("LiczbaBananow") + 25;
        PlayerPrefs.SetInt("LiczbaBananow", obecneBananki);
    }
    public void sklepIn()
    {
        Sklep.SetActive(true);
    }
    public void sklepOut()
    {
        Sklep.SetActive(false);
    }
    private void dailyReward()
    {
        dailyreward.SetActive(false);
        currentdata = System.DateTime.Now.ToString("MM/dd/yyyy");
        PlayerPrefs.SetString("currentdata", currentdata);
        Debug.Log(PlayerPrefs.GetString("currentdata"));
        if (PlayerPrefs.GetInt(currentdata)!=1)
        {
            PlayerPrefs.SetInt(currentdata, 1);
            obecneZycie = PlayerPrefs.GetFloat("Zycia") + 1;
            PlayerPrefs.SetFloat("Zycia", obecneZycie);
            dailyreward.SetActive(true);
        }
    }
    public void dailyRewardOff()
    {
        dailyreward.SetActive(false);
    }
}