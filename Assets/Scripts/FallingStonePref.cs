﻿using UnityEngine;
public class FallingStonePref : MonoBehaviour
{
    public GameObject myPrefab;
    private Transform transform;
    void Start()
    {
        transform = GetComponent<Transform>();
        InvokeRepeating("Spawn", 0f, 3f);
    }
    private void Spawn()
    {
        Instantiate(myPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
    }
}