﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
  public static void SavePlayer (PlayerController player)
    {   
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + player.levlelname + ".save";
        FileStream stream = new FileStream(path, FileMode.Create);

        Save save = new Save(player);
        formatter.Serialize(stream, save);
        stream.Close();
    }

    public static Save LoadPlayer (string levelname)
    {
        string path = Application.persistentDataPath + "/" + levelname + ".save";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Save save = formatter.Deserialize(stream) as Save;
            stream.Close();
            return save;
        }
        else
        {
            Debug.LogError("Nie znaleziono pliku do zapisu" + path);
            return null;
        }
    }


}
