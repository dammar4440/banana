﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clicksound : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioSource klik;
    void Start()
    {
        klik = GetComponent<AudioSource>();
    }
    public void clicking()
    {
        klik.Play();
    }
}
