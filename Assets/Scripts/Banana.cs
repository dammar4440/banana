﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Banana : MonoBehaviour
{
    
    public GameObject banana;
    private int aktualnystan;

    private void Awake()
    {
        if (PlayerPrefs.GetString(banana.name + SceneManager.GetActiveScene().name) == "false")
        {
            Destroy(gameObject);
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Player")
        {
           
            Destroy(gameObject);
            PlayerPrefs.SetString(banana.name + SceneManager.GetActiveScene().name, "false");
            aktualnystan = PlayerPrefs.GetInt("LiczbaBananow") + 1;
            PlayerPrefs.SetInt("LiczbaBananow", aktualnystan);
            Debug.Log("Liczba bananków: " + PlayerPrefs.GetInt("LiczbaBananow"));
            
        }
    }
}
