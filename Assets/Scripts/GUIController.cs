﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GUIController : MonoBehaviour
{
    [SerializeField]
    private GameObject settingsGO;
    [SerializeField]
    private GameObject pausewindowGO;
    [SerializeField]
    private GameObject bialawarstwaGO;

    private void Start()
    {
        ResumeGame();
        settingsGO.SetActive(false);
        pausewindowGO.SetActive(false);
        bialawarstwaGO.SetActive(false);
    }
    public void PauseWindowActive()
    {
        PauseGame();
        settingsGO.SetActive(false);
        pausewindowGO.SetActive(true);
        bialawarstwaGO.SetActive(true);
    }
    public void SettingsActive()
    {
        settingsGO.SetActive(true);
        pausewindowGO.SetActive(false);
        bialawarstwaGO.SetActive(true);
    }
    public void AllAreDisableds()
    {
        ResumeGame();
        settingsGO.SetActive(false);
        pausewindowGO.SetActive(false);
        bialawarstwaGO.SetActive(false);
    }
    private void PauseGame()
    {
        Time.timeScale = 0;
    }

    private void ResumeGame()
    {
        Time.timeScale = 1;
    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
        ResumeGame();
    }
}
