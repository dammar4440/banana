﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;using UnityEngine.Audio;
public class SetVolume : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixer efekty;

    public void SetLevel(float sliderValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(sliderValue)* 20);
    }
    public void SetLevelEfects(float sliderValue)
    {
        efekty.SetFloat("EffectVol", Mathf.Log10(sliderValue) * 20);
    }
}
